/*
 * Copyright (C) 2020 Daniel Frost
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import Ubuntu.Components.Popups 1.3
import io.thp.pyotherside 1.4
import QtNfc 5.2

Page {
    anchors.fill: parent

    header: PageHeader {
        id: pageHeader
        title: i18n.tr('uNFC')

        // trailingActionBar {
        //     actions: [
        //         Action {
        //             iconName: "help-contents"
        //             onTriggered: pageStack.push(Qt.resolvedUrl("About.qml"))
        //         }
        //     ]
        // }
    }

    Sections {
        id: header_sections
        // StyleHints {selectedSectionColor: top_text_color; }
        anchors {
            top: pageHeader.bottom
            topMargin: units.gu(1)
            horizontalCenter: parent.horizontalCenter
        }
        model: [i18n.tr("Messages"), i18n.tr("Manage"), i18n.tr("Instructions")]
    }

    Page {
        id: firstPage
        // contentHeight: messagesColumn.childrenRect.height

        // clip: true
        // flickableDirection: Flickable.AutoFlickIfNeeded
        anchors {
            top: header_sections.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        Column {
            id: messagesColumn
            visible: header_sections.selectedIndex === 0
            spacing: units.gu(2)

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                margins: units.gu(2)
            }

            Label {
                id: messagesLabel
                text: i18n.tr("No reading received yet.")
                wrapMode: Text.WordWrap
                width: parent.width
            }
        }
    }

    Page {
        id: secondPage
        // contentHeight: messagesColumn.childrenRect.height

        // clip: true
        // flickableDirection: Flickable.AutoFlickIfNeeded
        anchors {
            top: header_sections.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        Column {
            id: manageColumn
            visible: header_sections.selectedIndex === 1
            spacing: units.gu(2)

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                margins: units.gu(2)
            }

            Button {
                id: terminalButton
                anchors.horizontalCenter: parent.horizontalCenter
                text: i18n.tr("open terminal app to install 'qml-module-qtnfc'")
                onClicked: {
                    py.call("os.system",["ubuntu-app-launch com.ubuntu.terminal_terminal_0.9.9"],function (result) {
                        outputLabel.text = "";
                        outputLabel.text = result;
                    });
                }
            }
            //
            CheckBox {
                id: onoffSwitch
                height: units.gu(3)
                text: i18n.tr("Activate NFC")
                checked: true
                onCheckedChanged: toogleNFC(checked)
            }

            Label {
                id: headerLabel
                font.bold: true
                text: i18n.tr("Terminal Output")
            }

            ListItem {
                height: outputLabel.height + outputLabel.anchors.topMargin * 2
                divider.visible: false
                anchors {
                    left: parent.left
                    leftMargin: units.gu(2)
                }

                Label {
                    id: outputLabel

                    text: i18n.tr("Any output will be displayed below.")
                    font.family: "Ubuntu Mono"
                }
            }
        }
    }

    Page {
        id: thirdPage
        // contentHeight: infoColumn.childrenRect.height

        // clip: true
        // flickableDirection: Flickable.AutoFlickIfNeeded
        anchors {
            top: header_sections.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        Column {
            id: infoColumn
            visible: header_sections.selectedIndex === 2
            spacing: units.gu(2)

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                margins: units.gu(2)
            }

            Label {
                id: infoLabel
                text: i18n.tr("To use this test app the module 'qml-module-qtnfc' is needed. You may install it by opening a terminal window and running the following commands:")
                wrapMode: Text.WordWrap
                width: parent.width
            }
            Label {
                id: cmdOneLabel
                text: "sudo mount -o remount,rw /"
                font.italic: true
                wrapMode: Text.WordWrap
                width: parent.width
            }
            Label {
                id: cmdTwoLabel
                text: "sudo apt install qml-module-qtnfc"
                font.italic: true
                wrapMode: Text.WordWrap
                width: parent.width
            }
            Label {
                id: rebootLabel
                text: i18n.tr("Currently those steps need to be done after every update.")
                wrapMode: Text.WordWrap
                width: parent.width
            }
            Label {
                id: appstartLabel
                text: i18n.tr("NFC will get activated on app start. (not tested yet)")
                wrapMode: Text.WordWrap
                width: parent.width
            }
            Label {
                id: activateLabel
                text: i18n.tr("You can activate/deactivate NFC with the checkbox provided under 'Manage'. (not tested yet)")
                wrapMode: Text.WordWrap
                width: parent.width
            }
        }
    }

    //initializes the NFC component
    NearField {
        id: nfc
        filter: [ NdefFilter { type: "U"; typeNameFormat: NdefRecord.NfcRtd; minimum: 1; maximum: 1 } ]
        orderMatch: false

        //changes when messages get added or removed
        onMessageRecordsChanged: displayMessage(nfc.messageRecords)

        //changes when the adapter starts or stops polling
        // onPollingChanged: polling === true ? displayMessage(nfc.messageRecords): messagesLabel.text = i18n.tr("Adapter currently offline");
    }

    //initiate a python component for calls to python commands
    Python {
        id: py
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('./'));

            importModule('os', function() { console.log('python module os imported');});

            importModule('backend', function() {
                console.log("DEBUG: python loaded");
            });

            // setHandler('on-progress', function(value) {
            //     if (download_dialog.current !== null) {
            //         if (!download_dialog.current.progress_visible) {
            //             //console.log(download_dialog.current.progress_visible);
            //             download_dialog.current.progress_visible = true;
            //         }
            //         download_dialog.current.value = ((value * 2) / 100);
            //         //console.log(download_dialog.current.value);
            //     }
            // });
        }

        onError: {
            //console.log('Error: ' + traceback);
            var dialog = PopupUtils.open(errorDialog);
            dialog.traceback = traceback;
        }
    }

    Component {
        id: errorDialog

        Dialog {
            id: dialog
            title: i18n.tr("Error")
            text: i18n.tr("An error has occured: %1").arg(traceback)

            property string traceback: ""
            property string id_

            Button {
                id: closeButton
                text: i18n.tr("Close")
                onClicked: PopupUtils.close(dialog)
            }
        }
    }

    function displayMessage(messages) {
        messagesLabel.text = messages;
    }

    function toogleNFC(onoff) {
        var cmd = "dbus-send --system --dest=org.neard --print-reply /nfc0 org.freedesktop.DBus.Properties.Set string:org.neard.Adapter string:Powered variant:boolean:"
        py.call("os.system",[cmd + onoff],function (result) {
            outputLabel.text = "";
            outputLabel.text = result;
        });
    }

    Component.onCompleted: {
        toogleNFC(true)
        //firstRunTimer.start()
        outputLabel.text = i18n.tr("Any output will be displayed below.")
    }
    //
    // Timer {
    //     id: firstRunTimer
    //     interval: 100
    //     running: false
    //     repeat: false
    //     onTriggered: {
    //         // show about page on first app start
    //         if (settings.firstrun === true) {
    //             pageStack.push(Qt.resolvedUrl("About.qml"));
    //          }
    //     }
    // }
}
